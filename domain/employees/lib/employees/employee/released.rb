module Employees
  class Employee
    class Released < State
      def release
        raise NotHired
      end

      def hire
        yield
      end

      def hired
        transition_to Hired
      end
    end
    private_constant :Released
  end
end
