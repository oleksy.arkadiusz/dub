# :reek:MissingSafeMethod { exclude: [ name_set!, id_set! ] }
module Employees
  class Employee
    class State
      def initialize(employee)
        @employee = employee
      end

      def set_identification_document
        yield
      end

      def set_name
        yield
      end

      def id_set!; end

      def name_set!; end

      def identified; end

      private

      attr_reader :employee

      def transition_to(state)
        new_state = state.is_a?(State) ? state : state.new(employee)

        employee.transition_to(new_state)
      end
    end
    private_constant :State
  end
end
