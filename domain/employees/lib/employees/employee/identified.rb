module Employees
  class Employee
    class Identified < State
      def hire
        yield
      end

      def release
        raise NotHired
      end

      def hired
        transition_to Hired
      end
    end
    private_constant :Identified
  end
end
