module Employees
  class Employee
    class Hired < State
      def hire
        raise AlreadyHired
      end

      def release
        yield
      end

      def released
        transition_to Released
      end
    end
    private_constant :Hired
  end
end
