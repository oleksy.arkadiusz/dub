# :reek:MissingSafeMethod { exclude: [ name_set!, id_set! ] }
module Employees
  class Employee
    class Anonymous < State
      def initialize(employee)
        super

        @name_set = false
        @id_set = false
      end

      def hire
        raise NotIdentified
      end

      def name_set!
        self.name_set = true
      end

      def id_set!
        self.id_set = true
      end

      def identified
        transition_to Identified if name_set && id_set
      end

      private

      attr_accessor :name_set,
                    :id_set
    end
    private_constant :Anonymous
  end
end
