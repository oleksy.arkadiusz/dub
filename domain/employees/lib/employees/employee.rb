module Employees
  class Employee
    include AggregateRoot

    AlreadyHired  = Class.new(Error)
    NotHired      = Class.new(Error)
    NotIdentified = Class.new(Error)

    def initialize(employee_id)
      @employee_id = employee_id
      @state       = Anonymous.new(self)
    end

    def id
      employee_id
    end

    def name=(full_name)
      state.set_name do
        apply EmployeeFullNameSet.new(
          data: {
            employee_id: employee_id,
            full_name:   full_name
          }
        )
      end
    end

    def identity_document=(document)
      state.set_identification_document do
        apply EmployeeIdentityDocumentSet.new(
          data: {
            employee_id: employee_id,
            document:    document.to_h
          }
        )
      end
    end

    def hire(hired_on)
      state.hire do
        apply EmployeeHired.new(
          data: {
            employee_id: employee_id,
            hired_on:    hired_on
          }
        )
      end
    end

    def release(released_on)
      state.release do
        apply EmployeeReleased.new(
          data: {
            employee_id: employee_id,
            released_on: released_on
          }
        )
      end
    end

    on EmployeeHired do |_event|
      state.hired
    end

    on(EmployeeReleased) do |_event|
      state.released
    end

    on(EmployeeFullNameSet) do |_event|
      state.name_set!
      state.identified
    end

    on(EmployeeIdentityDocumentSet) do |_event|
      state.id_set!
      state.identified
    end

    def transition_to(new_state)
      self.state = new_state
    end

    private

    attr_reader :employee_id
    attr_accessor :state

    require_relative './employee/state'
    require_relative './employee/anonymous'
    require_relative './employee/identified'
    require_relative './employee/hired'
    require_relative './employee/released'
  end
end
