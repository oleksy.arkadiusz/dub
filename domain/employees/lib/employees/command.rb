require 'dry-struct'

class Command < Dry::Struct::Value
  InvalidCommand = Class.new(StandardError)

  def self.new(*args)
    super
  rescue Dry::Struct::Error => error
    raise InvalidCommand, error
  end
end
