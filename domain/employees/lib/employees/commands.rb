require_relative './types'
require_relative './command'

module Employees
  module Commands
  end
end

require_relative './commands/hire_new'
require_relative './commands/hire'
require_relative './commands/release'
