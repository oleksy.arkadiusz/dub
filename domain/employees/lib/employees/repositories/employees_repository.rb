module Employees
  class EmployeesRepository
    EVENT_STREAM = 'Employees$Employee$%<employee_id>s'.freeze

    def initialize(event_store)
      @repository = AggregateRoot::Repository.new(event_store)
    end

    def with_employee(employee_id, &blk)
      employee = Employee.new(employee_id)

      repository.with_aggregate(employee, stream(employee.id), &blk)
    end

    def find(employee_id)
      employee = Employee.new(employee_id)

      repository.load(employee, stream(employee.id))
    end

    def save(employee)
      repository.store(employee, stream(employee.id))
    end

    private

    attr_reader :repository

    def stream(employee_id)
      format(EVENT_STREAM, employee_id: employee_id)
    end
  end
end
