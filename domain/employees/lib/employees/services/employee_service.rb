module Employees
  class EmployeeService
    def initialize(employees_repository)
      @employees_repository = employees_repository
    end

    # :reek:FeatureEnvy
    def hire_new_employee(command)
      with_employee command.employee_id do |employee|
        employee.name = command.full_name
        employee.identity_document = command.identity_document
        employee.hire(command.hire_date)
      end
    end

    # :reek:FeatureEnvy
    def hire(command)
      with_employee command.employee_id do |employee|
        employee.hire(command.hire_date)
      end
    end

    # :reek:FeatureEnvy
    def release(command)
      with_employee command.employee_id do |employee|
        employee.release(command.release_date)
      end
    end

    private

    attr_reader :employees_repository

    def with_employee(employee_id, &blk)
      employees_repository.with_employee(employee_id, &blk)
    end
  end
end
