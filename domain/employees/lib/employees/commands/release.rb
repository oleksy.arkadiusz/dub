module Employees
  module Commands
    class Release < Command
      attribute :employee_id, Types::UUID_V4
      attribute :release_date, Types::Strict::DateTime
    end
  end
end
