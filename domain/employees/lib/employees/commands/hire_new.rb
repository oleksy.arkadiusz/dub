module Employees
  module Commands
    class HireNew < Command
      attribute :employee_id,       Types::UUID_V4
      attribute :full_name,         Types::Strict::String
      attribute :identity_document, Types.Instance(IdentityDocument)
      attribute :hire_date,         Types::Strict::DateTime
    end
  end
end
