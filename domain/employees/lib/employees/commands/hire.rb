module Employees
  module Commands
    class Hire < Command
      attribute :employee_id, Types::UUID_V4
      attribute :hire_date,   Types::Strict::DateTime
    end
  end
end
