module Employees
  class IdentityDocument
    class Documents
      IDENTITY_CARD = 'id_card'.freeze
      PASSPORT      = 'passport'.freeze

      KNOWN_DOCUMENTS = [IDENTITY_CARD, PASSPORT].freeze

      def self.known?(document_type)
        KNOWN_DOCUMENTS.include?(document_type)
      end
    end
    private_constant :Documents

    UnknownDocumentType = Class.new(Error)

    def self.passport(document_no)
      new(Documents::PASSPORT, document_no)
    end

    def self.identity_card(document_no)
      new(Documents::IDENTITY_CARD, document_no)
    end

    def initialize(document_type, document_no)
      raise UnknownDocumentType unless Documents.known?(document_type)

      @document_type = document_type
      @document_no = document_no
    end

    def to_h
      {
        document_type: document_type,
        document_no:   document_no
      }
    end

    private

    attr_reader :document_type, :document_no
  end
end
