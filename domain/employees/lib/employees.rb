require 'securerandom'
require 'aggregate_root'
require 'ruby_event_store'

module Employees
  class Error < StandardError; end

  require_relative './employees/version'
  require_relative './employees/identity_document'
  require_relative './employees/commands'
  require_relative './employees/types'
  require_relative './employees/domain_events'
  require_relative './employees/employee'
  require_relative './employees/repositories'
  require_relative './employees/services'

  module_function

  # :reek:TooManyStatements
  def setup(command_bus, event_store)
    employees_repository = EmployeesRepository.new(event_store)
    hire_employee_service = EmployeeService.new(employees_repository)

    command_bus.register(Commands::HireNew, ->(command) { hire_employee_service.hire_new_employee(command) })
    command_bus.register(Commands::Hire, ->(command) { hire_employee_service.hire(command) })
    command_bus.register(Commands::Release, ->(command) { hire_employee_service.release(command) })
  end
end
