lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'employees/version'

Gem::Specification.new do |spec|
  spec.name          = 'employees'
  spec.version       = Employees::VERSION
  spec.authors       = ['Arkadiusz Oleksy']
  spec.email         = ['oleksy.arkadiusz@gmail.com']

  spec.summary       = 'Employees context'
  spec.description   = 'Employees context'
  spec.homepage      = 'https://gitlab.com/oleksy.arkadiusz/dub'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rails_event_store-rspec', '~> 0.39.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_dependency 'aggregate_root', '~> 0.39.0'
  spec.add_dependency 'dry-struct', '~> 1.0'
  spec.add_dependency 'dry-types', '~> 1.0'
  spec.add_dependency 'ruby_event_store', '~> 0.39.0'
end
