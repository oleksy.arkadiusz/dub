module SpecSupport
  module EmployeeEvents
    def employee_full_name_set(employee_id, full_name)
      an_event(Employees::EmployeeFullNameSet).with_data(
        employee_id: employee_id,
        full_name:   full_name
      )
    end

    def employee_hired(employee_id, hired_on)
      an_event(Employees::EmployeeHired).with_data(
        employee_id: employee_id,
        hired_on:    hired_on
      )
    end

    def employee_released(employee_id, released_on)
      an_event(Employees::EmployeeReleased).with_data(
        employee_id: employee_id,
        released_on: released_on
      )
    end

    def employee_document_set(employee_id, identity_document)
      an_event(Employees::EmployeeIdentityDocumentSet).with_data(
        employee_id: employee_id,
        document:    identity_document.to_h
      )
    end
  end
end
