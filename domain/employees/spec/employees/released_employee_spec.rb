module Employees
  RSpec.describe Employee do
    describe 'released' do
      let(:employee_id) { SecureRandom.uuid }

      let :employee do
        described_class.new(employee_id).tap do |employee|
          employee.name = 'John Doe'
          employee.identity_document = IdentityDocument.identity_card('ABC123456')
          employee.hire(Date.today - 2)
          employee.release(Date.today - 1)
        end
      end

      it "can't be released" do
        expect { employee.release(Date.today) }
          .to raise_error(Employee::NotHired)
      end

      it 'can be hired again' do
        hire_date = Date.today
        employee.hire(hire_date)

        expect(employee).to have_applied(employee_hired(employee_id, hire_date))
      end

      it 'can change document' do
        id_document = IdentityDocument.passport('AB1234567')
        employee.identity_document = id_document

        expect(employee).to have_applied(employee_document_set(employee_id, id_document))
      end

      it 'can change name' do
        full_name = 'Jane Doe'
        employee.name = full_name

        expect(employee).to have_applied(employee_full_name_set(employee_id, full_name))
      end
    end
  end
end
