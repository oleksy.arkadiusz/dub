module Employees
  RSpec.describe Employee do
    describe 'hired' do
      let(:employee_id) { SecureRandom.uuid }

      let :employee do
        described_class.new(employee_id).tap do |employee|
          employee.name = 'John Doe'
          employee.identity_document = IdentityDocument.identity_card('ABC123456')
          employee.hire(Date.today - 1)
        end
      end

      it 'can be released' do
        release_date = Date.today
        employee.release(release_date)

        expect(employee).to have_applied(employee_released(employee_id, release_date))
      end

      it "can't be hired again" do
        expect { employee.hire(Date.today) }
          .to raise_error(Employee::AlreadyHired)
      end

      it 'can change document' do
        id_document = IdentityDocument.passport('AB1234567')
        employee.identity_document = id_document

        expect(employee).to have_applied(employee_document_set(employee_id, id_document))
      end

      it 'can change name' do
        full_name = 'Jane Doe'
        employee.name = full_name

        expect(employee).to have_applied(employee_full_name_set(employee_id, full_name))
      end
    end
  end
end
