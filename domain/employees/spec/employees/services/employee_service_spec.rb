module Employees
  RSpec.describe EmployeeService do
    subject(:service) { described_class.new(employees_repository) }

    let(:employees_repository) { EmployeesRepository.new(event_store) }
    let(:event_store) { RubyEventStore::Client.new(repository: event_store_repository) }
    let(:event_store_repository) { RubyEventStore::InMemoryRepository.new }

    describe 'hire new employee' do
      let :command do
        Commands::HireNew.new(
          employee_id:       SecureRandom.uuid,
          full_name:         'John Doe',
          identity_document: IdentityDocument.identity_card('ABC123456'),
          hire_date:         DateTime.now
        )
      end

      it 'sets user data and hires him' do
        service.hire_new_employee(command)

        expect(event_store).to have_published(
          employee_full_name_set(command.employee_id, command.full_name),
          employee_document_set(command.employee_id, command.identity_document),
          employee_hired(command.employee_id, command.hire_date)
        )
      end
    end

    describe 'hire existing employee' do
      let(:employee_id) { SecureRandom.uuid }

      let :command do
        Commands::Hire.new(
          employee_id: employee_id,
          hire_date:   DateTime.now
        )
      end

      before do
        employee = Employee.new(employee_id)
        employee.name = 'John Doe'
        employee.identity_document = IdentityDocument.identity_card('ABC123456')
        employees_repository.save(employee)
      end

      it 'hires user' do
        service.hire(command)

        expect(event_store).to have_published(
          employee_hired(command.employee_id, command.hire_date)
        )
      end
    end

    describe 'release hired user' do
      let(:employee_id) { SecureRandom.uuid }

      let :command do
        Commands::Release.new(
          employee_id:  employee_id,
          release_date: DateTime.new(2019, 9, 30)
        )
      end

      before do
        service.hire_new_employee Commands::HireNew.new(
          employee_id:       employee_id,
          full_name:         'John Doe',
          identity_document: IdentityDocument.identity_card('ABC123456'),
          hire_date:         DateTime.new(2019, 1, 1)
        )
      end

      it 'releases employee' do
        service.release command

        expect(event_store).to have_published(
          employee_released(command.employee_id, command.release_date)
        )
      end
    end
  end
end
