module Employees
  RSpec.describe Employee do
    describe 'anonymous' do
      let(:employee_id) { SecureRandom.uuid }
      let(:employee) { described_class.new(employee_id) }

      it 'can have full name set' do
        full_name = 'John Doe'
        employee.name = full_name

        expect(employee).to have_applied(employee_full_name_set(employee_id, full_name))
      end

      it 'can be identified by identity card' do
        id_card = IdentityDocument.identity_card('ABC123456')
        employee.identity_document = id_card

        expect(employee).to have_applied(employee_document_set(employee_id, id_card))
      end

      it 'can be identified by passport' do
        passport = IdentityDocument.passport('AB1234567')
        employee.identity_document = passport

        expect(employee).to have_applied(employee_document_set(employee_id, passport))
      end

      it "can't be hired" do
        expect { employee.hire(Date.today) }
          .to raise_error(Employee::NotIdentified)
      end
    end
  end
end
