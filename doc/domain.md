# HUB

Currently in our company we got central hub where we can track on what activities in which projects we spent time. Basing on this we're able to generate reports for clients and our accounting team is able to calculate salaries and prepare an invoice for client. The app also keeps track of employees career level, contracts etc. Currently existing application is almost standard rails app (with few layers added), not split into diffrent contexts / modules.

This document is meant to be a work in progress and will be updated upon domain exploration.

Treat value objects as example value objects that i was able to define now. More of them will emerge while implementing actual context / domain

Core domain is probably everything connected with activity tracking as it is basis for paychecks and invoices calculations.

These are the bounded contexts i was able to identify at the moment:

## Employees

### Description:
Context used by our HR team to keep track of current (but also prevoius) employees. In this context actual employee / contractor will be added (addres / contact information, Personal Data)

### Entities / Aggregates:
* `Employee`

### Value Objects:
* `Address`
* `Document` (type and number)
* `BirthDate`
* `FullName`

---

## Careers

### Description:
Context for keeping track of employees carrer progress. We not only have a level (junir / mid / senior) but also tags that impacts our current carrer status. More on this can be found for example in this [article](https://geek.justjoin.it/system-wynagrodzen-byc-prosty-czesto-aktualizowany/) (written in polish) - take a look at the [diagram](https://geek.justjoin.it/wp-content/uploads/2018/04/sf_table_final.png) to take a grasp on tags.

### Entities / Aggregates:
* `Career`

### Value Objects:
* `Tag` (having its own level and type)
* `CareerLevel` (Something like junior / mid / senior)
* `Position`
* `TechStack`
* `Technology`

---

## Projects

### Description:
As a software house we have many ongoing projects so i figured out that this would be context for keeping track of them. Here would probably be the place where Employee assignements to projects will be done (or should it be a separate context). I decided to add aggregate for employee asignements (can't figure out good name for this) to add some logic deciding on how many projects and in what roles can be handled by single employee (this is something that is not present in current system but i think it can help when scheduling new projects later on)

### Entities / Aggregates:
* `Project`
* `EmployeeAsignments`

### Value Objects:
* `Owner` (still thinking if this can be a value object in this context)
* `Role` (what role would assigned employee will have in project)
* `AssignementPeriod`

---

## Accounting

### Description
A context for handling stuff with salaries and client invoices calculations (This is the place where i have least knowledge for now, but when i'll have some more free time at work, i'll try to take someone from accounting and talk about this)

### Entities / Aggregates
* `EmployeeContract`
* `ProjectContract`

### Value Objects:
* `Money`
* `BankAccount`

---

## Activity Tracking / Time Tracking

### Description:
Context for activity tracking. Except from tracking employees activities (time spent on client projects / internal projects / conferences / etc) here will be also possibility to define activities types. This is the context i have some problems with. I would need to validate activity with for example available quota for user. `ActivityType` should also have something like billing ratio (how much of time spent can be billed in percentage. For example, we would like to allow employee to take a sick leave which we will pay him 50% but he should have a quota of this activity type available). I have a feeling that activity type billing settings should be in accounting context.

### Entities / Aggregates:
* `ActivityType`
* `ActivityTrack`
* `EmployeeActivityQuotas`

### Value Objects:
* `TimeSpent`
* `Day`

---

## Identity & Access control

### Description:
Context primarily for managing employees roles. User should be created when new employee is added

### Entities / Aggregates
* `Organization` (as for now, not sure about this one, but would be cool if app was able to run in Saas mode)
* `User`

### Value Objects:
* `Role`

---

I havent put anything like reporting because i think it will be handled by separate read models per report type probably.
